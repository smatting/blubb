from docopt import docopt
from .model import TransformerLanguageModel
from .data import TextDataFromFile, CharIndexer
import os
import sys
import readline
import atexit


def maybe_save(model, data, save_folder, save_interval):
    def f(epoch, loss, force=False):
        if force or ((epoch % save_interval) == 0):

            filename = os.path.join(save_folder, 'epoch_{:05}_loss_{:07.4f}.atmodel'.format(epoch, loss))
            print('Saving model: ' + filename)
            meta = {
                'chars': data.chars,
                'epoch': epoch,
            }
            model.save_file(filename, meta=meta)
    return f

def train(filename, d_model, N, heads, dropout, lsr_epsilon, batch_size, warmup_steps, save_interval, start_file=None, start_iter=None, save_folder=None):
    data = TextDataFromFile(filename)

    if start_file is not None:
        model = TransformerLanguageModel.load_file(start_file)
        if start_iter is not None:
            start_iter = int(start_iter)
        elif model._meta['epoch'] is not None:
            start_iter = int(model._meta['epoch']) + 1
    else:
        start_iter = 1
        model = TransformerLanguageModel(data.char_indexer.dimensions(), d_model, N, heads, dropout, lsr_epsilon)

    batches = data.get_batches(batch_size)

    save_f = maybe_save(model, data, save_folder, save_interval)

    model.train(batches, warmup_steps=warmup_steps, maybe_save=save_f, start_epoch=start_iter)

    print('Stopped.')

def test(model_file):
    import readline
    import atexit

    histfile = os.path.expanduser('~/.askparrot_history')
    histfile_size = 1000

    try:
        readline.read_history_file(histfile)
        readline.set_history_length(histfile_size)
    except FileNotFoundError:
        pass

    atexit.register(readline.write_history_file, histfile)

    model = TransformerLanguageModel.load_file(model_file)

    char_indexer = CharIndexer(model._meta['chars'])

    while True:
        try:
            user_input = input('You:\n')
            print()
            x = char_indexer.text_to_tensor(user_input, close=False).unsqueeze(0)

            y = model.predict_random(x, max_length=1500)

            s = char_indexer.tensor_to_text(y)

            print('Parrot:\n' + s + '\n')

        except Exception as e:
            print('Error:' + str(e))
        
        sys.stdout.flush()


def main():
    '''
Usage:
    askparrot train <data_file> [--start-file=<f>] [--start-iter=<i>] [--save-folder=<p>]
    askparrot test <model_file>

Options:
    --start-file=<f>      File with initial model parameters.
    --start-iter=<n>      Start iteration. [default: 1]
    --save-folder=<p>     Path for saving model files. [default: .]
    '''
    args = docopt(main.__doc__)

    if args['train']:
        # TODO make this configurable
        train(args['<data_file>'],
        d_model=256,
        N=3,
        heads=8,
        dropout=0.1,
        lsr_epsilon=0.1,
        batch_size=20,
        warmup_steps=500,
        save_interval=100,
        start_file=args['--start-file'],
        start_iter=args['--start-iter'],
        save_folder=args['--save-folder']
        )
    elif args['test']:
        test(args['<model_file>'])


if __name__ == '__main__':
    main()
