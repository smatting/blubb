import torch
from torch.nn.utils.rnn import pad_sequence
import glob
import os
import functools
from .model import Batch
from .global_settings import USE_GPU


class CharIndexer(object):
    def __init__(self, chars):
        self.chars = chars
        self.char_to_idx = {c: i for i, c in enumerate(self.chars, 3)}
        self.idx_to_char = {i: c for i, c in enumerate(self.chars, 3)}

        # index 0 is reserved for padding
        # index 1 is reserved for "start of sequence"
        # index 2 is reserved for "end of sequence"
        self.pad         = 0
        self.sos         = 1
        self.eos         = 2
        self.char_to_idx = {c: i for i, c in enumerate(self.chars, 3)}
        self.idx_to_char = {i: c for i, c in enumerate(self.chars, 3)}

    def text_to_tensor(self, text, close=True):
        seq = [self.sos] + [self.char_to_idx[c] for c in text]
        if close:
            seq += [self.eos]

        x = torch.LongTensor(seq)

        if USE_GPU:
            # copy to page locked memory
            x = x.pin_memory()

            # copy async to GPU
            x = x.cuda(non_blocking=True)

        return x
    
    def tensor_to_text(self, x):
        x = [i for i in x.flatten().tolist() if i > self.eos]
        return ''.join([self.idx_to_char[i] for i in x])

    def dimensions(self):
        return len(self.chars) + 3


class TextData(object):
    def __init__(self, texts):
        self.texts = texts
    
    def get_batches(self, batch_size):
        batch_bounds = list(range(len(self.texts)))[0::batch_size] + [len(self.texts)]
        batch_starts = batch_bounds[:-1]
        batch_ends   = batch_bounds[1:]

        batches = []
        for i in range(len(batch_starts)):
            texts   = self.texts[batch_starts[i]:batch_ends[i]]
            tensors = [self.char_indexer.text_to_tensor(t) for t in texts] 
            batches.append(pad_sequence(tensors, batch_first=True, padding_value=self.char_indexer.pad))

        return [Batch(b) for b in batches]


class TextDataFromFile(TextData):
    def __init__(self, filename, textsep=None):
        if textsep is None:
            textsep = '--------------------------------------------------------------------------------'
        
        with open(filename, 'r') as f:
            s = f.read()
        
        texts = sorted(s.split(textsep), key=lambda x: len(x))
        super(TextDataFromFile, self).__init__(texts)

        self.chars = sorted(functools.reduce(lambda x, y: x | y, [set(t) for t in self.texts]))

        self.char_indexer = CharIndexer(self.chars)


class TextDataFromFolder(TextData):
    def __init__(self, path):
        files = glob.glob(os.path.join(path, '*.txt'))
    
        self.texts = []
        for file in files:
            with open(file, 'r') as f:
                self.texts.append(f.read())

        texts = sorted(self.texts, key=lambda x: len(x))
        super(TextDataFromFolder, self).__init__(texts)

        self.chars        = sorted(functools.reduce(lambda x, y: x | y, [set(t) for t in self.texts]))

        self.char_indexer = CharIndexer(self.chars)
