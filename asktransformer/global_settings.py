import os
import torch


USE_GPU = None
USE_FP16 = None


def retrieve_compute_settings():
    global USE_GPU

    if USE_GPU is None:
        USE_GPU = os.environ.get('USE_GPU')
        if USE_GPU is None:
            USE_GPU = False
        else:
            assert USE_GPU in ['0', '1'], 'USE_GPU has to be 0 or 1.'
            USE_GPU = (USE_GPU == '1')
        
        if USE_GPU:
            assert torch.cuda.is_available(), 'USE_GPU=1 but GPU is not available.'

    global USE_FP16

    if USE_FP16 is None:
        USE_FP16 = os.environ.get('USE_FP16')
        if USE_FP16 is None:
            USE_FP16 = False
        else:
            assert USE_FP16 in ['0', '1'], 'USE_FP16 has to be 0 or 1.'
            USE_FP16 = (USE_FP16 == '1')

            if USE_FP16:
                assert USE_GPU, 'Float16 is only fully supported on GPU.'


def set_default_tensor():
    retrieve_compute_settings()

    if USE_GPU:
        tor = torch.cuda
    else:
        tor = torch

    if USE_FP16:
        torch.set_default_tensor_type(tor.HalfTensor)
    else:
        torch.set_default_tensor_type(tor.FloatTensor)


set_default_tensor()

