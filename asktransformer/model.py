from .global_settings import USE_GPU
import torch
import torch.nn
from torch.nn.parameter import Parameter
from torch.autograd import Variable
from torch.optim import Adam
import numpy as np
import random
import math
from collections import OrderedDict
import json


def attention(query, key, value, mask=None, dropout=None):
    # dimensionality of key/query
    d_k = query.size(-1)

    # for each sample in batch, calculate attention scores for each query w.r.t. each key,
    # i.e. Q*K.T (and additionally normalized)
    # Resulting shape: batch_size * sequence_length * sequence_length
    scores = torch.matmul(query, key.transpose(-2, -1)) / math.sqrt(d_k)

    if mask is not None:
        # kind of a hack but works: do not pay attention to masked elements by setting
        # attention scores to a very small value (so they become effectively zero after
        # the softmax).
        scores = scores.masked_fill(mask == 0, -1e9)

    # Convert attention scores into probabilistic weights by putting them through a softmax
    p_attn = torch.nn.functional.softmax(scores, dim=-1)

    if dropout is not None:
        p_attn = dropout(p_attn)

    # calculate query result, which is the weighted sum of the values in our "lookup table"
    # using the weights we just calculated.
    # attn has the format: batch_size * value_dim
    attn = torch.matmul(p_attn, value)

    return attn, p_attn


def subsequent_mask(size):
    '''
    For each position, mask out all subsequent position, so that we don't
    use them during training for predicting the next label.
    '''
    return torch.tril(torch.ones(size, size, dtype=torch.uint8)).unsqueeze(0)


class MultiHeadedAttention(torch.nn.Module):
    def __init__(self, heads, d_model, dropout=0.1, bias=False):
        '''
        heads:   Number of heads
        d_model: Dimensionality of internal representation (more or less the equivalent to the hidden dimension in seq2seq models).
                 It should be chosen in a way that d_model is a multiple of the number of heads, since we use a reduced dimensionality
                 for each head (d_model / heads) and just stack the results together to get the original dimension.
        bias:    In the article http://nlp.seas.harvard.edu/2018/04/03/attention.html they say we use projection matrices, i.e. no bias.
                 But: the implementation in the code below doesn't turn off the biases. I assume it's a mistake, but anyway make it parameterizable.
                 Intuitively, I don't think we can profit much from a bias here.
        '''
        super(MultiHeadedAttention, self).__init__()

        assert d_model % heads == 0

        self.heads   = heads
        self.d_model = d_model
        
        # we use a reduced dimensionality for each head
        self.d_head  = d_model // heads

        # TODO proper initializations (xavier?)

        # Linear projections for the queries for all heads
        # W_Q = [W_Q_1, W_Q_2, ..., W_Q_heads]
        self.W_Q     = torch.nn.Linear(d_model, d_model, bias=bias)

        # Linear projections for the keys for all heads
        # W_K = [W_K_1, W_K_2, ..., W_K_heads]
        self.W_K     = torch.nn.Linear(d_model, d_model, bias=bias)

        # Linear projections for the values for all heads
        # W_V = [W_V_1, W_V_2, ..., W_V_heads]
        self.W_V     = torch.nn.Linear(d_model, d_model, bias=bias)

        # Final linear mixing for the concatenated dot-product attention outputs for all heads
        self.W_O     = torch.nn.Linear(d_model, d_model, bias=bias)

        self.dropout = torch.nn.Dropout(p=dropout)

    def forward(self, query, key, value, mask=None):
        '''
        query: batch_size * sequence_length * (key_dim * heads)
        key:   batch_size * sequence_length * (key_dim * heads)
        value: batch_size * sequence_length * (value_dim * heads)
        mask:  batch_size * sequence_length

        NOTE key_dim and value_dim are for simplicity (and efficiency) reasons chosen as:

             key_dim = value_dim = d_model / heads = d_head
        '''
        if mask is not None:
            # Add extra dimension for the different heads
            mask = mask.unsqueeze(1)
        
        nbatches = query.size(0)

        # Apply linear projections for all heads.
        # Afterwards, for each sample in the batch,
        # we have the V, K and Q values for each head stacked row-wise.
        # Example (of course, same dimensionality, but different interpretation):
        #
        # query:            batch_size * sequence_length * d_model
        # query_projected:  batch_size * sequence_length * (d_head * heads)
        query_projected = self.W_Q(query)
        key_projected   = self.W_K(key)
        value_projected = self.W_V(value)

        # Reshape tensors so that we have an extra dimension enumerating over the heads.
        # Result: batch_size * sequence_length * heads * d_head
        #
        # Afterwards, swap dimension 1 and 2 so that the final dimensions of the tensors are:
        # batch_size * heads * sequence_length * d_head
        query_heads = query_projected.view(nbatches, -1, self.heads, self.d_head).transpose(1, 2)
        key_heads   = key_projected.view(nbatches, -1, self.heads, self.d_head).transpose(1, 2)
        value_heads = value_projected.view(nbatches, -1, self.heads, self.d_head).transpose(1, 2)

        # Apply attention.
        attn, p_attn = attention(query_heads, key_heads, value_heads, mask=mask, dropout=self.dropout)

        # Convert back to "stacked format":
        # batch_size * sequence_length * (d_head * heads)
        attn = attn.transpose(1, 2).contiguous().view(nbatches, -1, self.heads * self.d_head)

        # Apply final linear mixing matrix
        out = self.W_O(attn)

        return out


class LayerNorm(torch.nn.Module):
    '''
    Layer normalization.

    https://arxiv.org/abs/1607.06450
    '''
    def __init__(self, features, eps=1e-6):
        super(LayerNorm, self).__init__()
        self.a_2 = torch.nn.Parameter(torch.ones(features))
        self.b_2 = torch.nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        # calculate mean and std w.r.t. last dimension, which is the "feature dimension".
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.a_2 * (x - mean) / (std + self.eps) + self.b_2


class SubLayer(torch.nn.Module):
    def __init__(self, d_model, dropout=0.1):
        super(SubLayer, self).__init__()
        self.norm    = LayerNorm(d_model)
        self.dropout = torch.nn.Dropout(dropout)
    
    def forward(self, x, f):
        # NOTE Instead of LayerNorm(x + Sublayer(x)), which is mentioned in the paper,
        #      we implement x + Sublayer(LayerNorm(x)). Discussions in the thread below
        #      http://nlp.seas.harvard.edu/2018/04/03/attention.html and
        #      https://github.com/OpenNMT/OpenNMT-py/blob/cd29c1dbfb35f4a2701ff52a1bf4e5bdcf02802e/onmt/encoders/transformer.py#L48 and
        #      https://disq.us/url?url=https%3A%2F%2Fgithub.com%2FOpenNMT%2FOpenNMT-py%2Fissues%2F770%3AP4vM-d65dWitBlBZMRrqtd4pcFw&cuid=5460729
        #      show that it seems to be intentional and empirically better. So we do it like that as well.
        #      Also note that the position of the dropout is weird. But it seems to be like that in the reference implementation.
        return x + self.dropout(f(self.norm(x)))


class PositionwiseFeedForward(torch.nn.Module):
    '''
    Position-wise feed forward is applied after the attention layer, basically to add
    some complexity to the model, especially also non-linearity through ReLU units
    (the result of the attention layer is just a weighted sum of the inputs and therefore
    linear in the inputs).
    '''
    def __init__(self, d_model, d_ff, dropout=0.1, bias=True):
        super(PositionwiseFeedForward, self).__init__()
        self.w_1 = torch.nn.Linear(d_model, d_ff, bias=bias)
        self.w_2 = torch.nn.Linear(d_ff, d_model, bias=bias)
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, x):
        return self.w_2(self.dropout(torch.nn.functional.relu(self.w_1(x))))


class TransformerEncoderLayer(torch.nn.Module):
    def __init__(self, heads, d_model, d_ff, dropout=0.1):
        super(TransformerEncoderLayer, self).__init__()
        self.self_attn     = MultiHeadedAttention(heads, d_model, dropout=dropout)
        self.feed_forward  = PositionwiseFeedForward(d_model, d_ff, dropout=dropout)
        self.sublayer_attn = SubLayer(d_model, dropout)
        self.sublayer_ff   = SubLayer(d_model, dropout)

    def forward(self, x, mask):
        # Self-attention sublayer
        x = self.sublayer_attn(x, lambda x: self.self_attn(x, x, x, mask))

        # Position-wise feed forward sublayer
        x = self.sublayer_ff(x, self.feed_forward)

        return x


class TransformerEncoder(torch.nn.Module):
    def __init__(self, N, heads, d_model, d_ff, dropout=0.1):
        '''
        N: number of layers
        '''
        super(TransformerEncoder, self).__init__()
        self.layers = torch.nn.ModuleList([TransformerEncoderLayer(heads, d_model, d_ff, dropout) for i in range(N)])
        self.norm   = LayerNorm(d_model)
    
    def forward(self, x, mask):
        for layer in self.layers:
            x = layer(x, mask)

        return self.norm(x)


class TransformerDecoderLayer(torch.nn.Module):
    def __init__(self, heads, d_model, d_ff, dropout=0.1, use_src_attn=True):
        '''
        use_src_attn: Use source attention. This is always the case, if we use
                  the transformer decoder in an encoder-decoder scenario.
                  But sometimes (for example: generative scenario) we just want
                  the decoder by itself. In such cases set src_attn=False.
        '''
        super(TransformerDecoderLayer, self).__init__()
        self.use_src_attn = use_src_attn

        # self attention
        self.self_attn          = MultiHeadedAttention(heads, d_model, dropout=dropout)
        self.sublayer_self_attn = SubLayer(d_model, dropout)

        # source attention
        if self.use_src_attn:
            self.src_attn          = MultiHeadedAttention(heads, d_model, dropout=dropout)
            self.sublayer_src_attn = SubLayer(d_model, dropout)

        # position-wise feed forward
        self.feed_forward  = PositionwiseFeedForward(d_model, d_ff, dropout=dropout)
        self.sublayer_ff   = SubLayer(d_model, dropout)

    def forward(self, x, tgt_mask, memory=None, src_mask=None):
        # Self-attention sublayer
        x = self.sublayer_self_attn(x, lambda x: self.self_attn(x, x, x, tgt_mask))

        if self.use_src_attn:
            x = self.sublayer_src_attn(x, lambda x: self.src_attn(x, memory, memory, src_mask))

        x = self.sublayer_ff(x, self.feed_forward)

        return x


class TransformerDecoder(torch.nn.Module):
    def __init__(self, N, heads, d_model, d_ff, dropout=0.1, use_src_attn=True):
        super(TransformerDecoder, self).__init__()
        self.layers = torch.nn.ModuleList([TransformerDecoderLayer(heads, d_model, d_ff, dropout, use_src_attn) for i in range(N)])
        self.norm   = LayerNorm(d_model)

    def forward(self, x, tgt_mask, memory=None, src_mask=None):
        for layer in self.layers:
            x = layer(x, tgt_mask, memory, src_mask)

        return self.norm(x)


class PositionalEncoding(torch.nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = torch.nn.Dropout(p=dropout)
        
        # Compute the positional encodings once in log space.
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0., max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0., d_model, 2) * -(math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)

        # Make positional encodings persistent on the device in which the model is (e.g. CUDA),
        # but don't treat it as a parameter (so do not optimize it).
        self.register_buffer('pe', pe)
        
    def forward(self, x):
        x = x + Variable(self.pe[:, :x.size(1)], requires_grad=False)
        return self.dropout(x)


class InputOutputEmbedding(torch.nn.Module):
    '''
    (Optionally tied) input-output embeddings.

    https://arxiv.org/pdf/1608.05859.pdf
    '''
    def __init__(self, d_symbols, d_model, tied=True):
        super(InputOutputEmbedding, self).__init__()
        self.d_symbols = d_symbols
        self.d_model   = d_model

        #self.weight_in = Parameter(torch.Tensor(d_model, d_symbols))
        self.weight_in = Parameter(torch.Tensor(d_symbols, d_model))
        torch.nn.init.xavier_uniform_(self.weight_in)

        if tied:
            self.weight_out = self.weight_in
        else:
            self.weight_out = Parameter(torch.Tensor(d_symbols, d_model))
            torch.nn.init.xavier_uniform_(self.weight_out)
    
    def forward(self, x, input_embed=True):
        '''
        input_embed: if true, we perform an input embedding, otherwise an output embedding.
        '''
        if input_embed:
            # TODO understand if and why math.sqrt(d_model) should be the right scaling factor here
            return torch.nn.functional.embedding(x, self.weight_in) * math.sqrt(self.d_model)
        else:
            x = torch.nn.functional.linear(x, self.weight_out)
            return torch.nn.functional.log_softmax(x, dim=-1)


class GenerativeTransformer(torch.nn.Module):
    def __init__(self, N, heads, d_symbols, d_model, d_ff, dropout=0.1):
        super(GenerativeTransformer, self).__init__()
        self.decoder  = TransformerDecoder(N, heads, d_model, d_ff, dropout, False)
        self.embed    = InputOutputEmbedding(d_symbols, d_model)
        self.position = PositionalEncoding(d_model, dropout)

        # some might be already initialized - but we enforce xavier init here for all non-vectors.
        for p in self.parameters():
            if p.dim() > 1:
                torch.nn.init.xavier_uniform_(p)

    def forward(self, x, mask=None):
        x = self.position(self.embed(x))
        x = self.decoder(x, mask)
        x = self.embed(x, input_embed=False)

        return x


class Batch(object):
    def __init__(self, x, padding_value=0):
        self.x      = x
        
        self.input  = x[:, :-1]
        self.target = x[:, 1:]

        # filter actual sequences and add empty dimension for "attention position" dimension,
        # since we combine it with the subsequent mask afterwards
        self.mask   = (self.input != padding_value).unsqueeze(-2) & subsequent_mask(self.input.size(-1))


class KLDivLossLSR(torch.nn.Module):
    '''
    KL-divergence loss including label smoothing regularization.
    See:
        * Original paper:

            https://arxiv.org/abs/1512.00567

        * For comparison of this and other methods on different tasks:

            https://arxiv.org/abs/1701.06548
    '''
    def __init__(self, input_size, lsr_epsilon, start_idx=2):
        '''
        start_idx: Index from which on we assign positive weights (previous ones are for example padding, SOS, etc.)

        Note: Index 0 is assumed to be used for padding.
        '''
        super(KLDivLossLSR, self).__init__()
        self.input_size  = input_size
        self.lsr_epsilon = lsr_epsilon
        self.start_idx   = start_idx

        # Calculate soft weights
        rest_weights = lsr_epsilon / (self.input_size - start_idx - 1)
        targets = ((1.0 - lsr_epsilon) - rest_weights) * torch.eye(self.input_size) + rest_weights
        
        # Assign target probabilities of 0 for special dimensions
        targets[:, :start_idx] = 0
        targets[:start_idx, :] = 0

        self.embed_target = torch.nn.Embedding.from_pretrained(targets, freeze=True)

    def forward(self, input, target, normalize_sequence_lengths=True):
        '''
        input:  Format * x N x D ; containing *log* probabilities (!)
        target: Format * x N ; containing indices of target labels.
        '''
        target_smooth = self.embed_target(target)

        kl = torch.nn.functional.kl_div(input, target_smooth, reduction='none')

        # TODO figure out which method works better during training
        if normalize_sequence_lengths:
            # Normalize w.r.t. sequence lengths. This gives *every sample* the same importance/weight.
            # 
            # What we want to have is:
            # 1. Summing over the last dimension (since these are the addends of the KL-divergence sum).
            #    As a result, we a get a reduced tensor with KL-divergences.
            # 2. Averaging over the previous last dimension (which is now the last remaining dimension),
            #    but considering the sequence lengths while doing that.
            # 3. Average by number of samples in the batch.

            # NOTE we assume here that 0 is the padding dimension
            lengths = (target != 0).sum(-1).type(kl.dtype)
            kl      = kl.sum(-1).sum(-1) / lengths
        else:
            # Do not normalize w.r.t. sequence lengths. This gives *every single label* the same importance/weight.
            #
            # What we want to have is:
            # 1. Summing over the last dimension (since these are the addends of the KL-divergence sum).
            #    As a result, we a get a reduced tensor with KL-divergences.
            # 2. Summing over the previous last dimension (which is now the last remaining dimension),
            #    since we do not normalize.
            # 3. Average by number of samples in the batch.
            kl = kl.sum(-1).sum(-1)

        return kl.mean()


class TransformerLanguageModel(object):
    def __init__(self, d_symbols, d_model, N=6, heads=8, dropout=0.1, lsr_epsilon=0.1):
        self.d_symbols   = d_symbols
        self.d_model     = d_model
        self.N           = N
        self.heads       = heads
        self.dropout     = dropout
        self.lsr_epsilon = lsr_epsilon

        self.d_ff        = d_model * 4

        self.model       = GenerativeTransformer(self.N, self.heads, self.d_symbols, self.d_model, self.d_ff, self.dropout)
        self.loss        = KLDivLossLSR(d_symbols, lsr_epsilon)

        # TODO maybe use Noam Optimizer if this doesn't work well
        self.optim       = Adam(self.model.parameters(), betas=(0.9, 0.98), eps=1e-9)

        if USE_GPU:
            self.model.cuda()
            self.loss.cuda()
    
    def train_batch(self, batch, learning_rate):
        self.optim.zero_grad()
        for g in self.optim.param_groups:
            g['lr'] = learning_rate

        out = self.model.forward(batch.input, batch.mask)
        loss = self.loss(out, batch.target)

        loss.backward()

        self.optim.step()

        return loss

    def train_epoch(self, batches, learning_rate):
        self.model.train()

        losses = []

        random.shuffle(batches)
        for b in batches:
            loss = self.train_batch(b, learning_rate=learning_rate)
            losses.append(float(loss))
        
        return np.mean(losses)
    
    def train(self, batches, start_epoch=None, end_epoch=None, warmup_steps=100, maybe_save=None):
        if start_epoch is None:
            start_epoch = 1
        
        if end_epoch is None:
            end_epoch = np.inf

        i = start_epoch
        while i < end_epoch:
            try:
                lr = self.step_size(i, warmup_steps, 2.)
                loss = self.train_epoch(batches, learning_rate=lr)
                print('Finished epoch {}. Loss: {}. LR={:.6f}'.format(i, loss, lr))
                if maybe_save is not None:
                    maybe_save(i, loss)
                i += 1
            except RuntimeError:
                # try to free cuda cache
                if USE_GPU:
                    torch.cuda.empty_cache()
                    print('Emptying cache...')
            except KeyboardInterrupt:
                if maybe_save is not None:
                    maybe_save(i, loss, force=True)
                    break

    def step_size(self, epoch, warmup_steps, factor):
        return factor * (self.d_model ** (-0.5)) * min(epoch ** -0.5, epoch * warmup_steps ** (-1.5))

    def predict_step(self, x):
        mask = subsequent_mask(x.size(-1))

        out = self.model.forward(x, mask=mask)

        return out
    
    def predict_random(self, x=None, max_length=100):
        self.model.eval()

        if x is None:
            x = torch.LongTensor([[1]])  # TODO implement that better; 1=SOS
            if USE_GPU:
                x = x.cuda()
        
        choice_list = list(range(self.d_symbols))
        xs = [x]
        for i in range(max_length):
            out = self.predict_step(x)[0, -1, :]
            p = np.exp(out.detach().cpu().numpy()) ** 3
            p /= p.sum()
            choice = np.random.choice(choice_list, p=p)
            x_new = torch.LongTensor([[choice]])
            if USE_GPU:
                x_new = x_new.cuda()
            xs.append(x_new)
            x = torch.cat(xs, dim=1)

            if choice == 2:  # TODO change that
                break

        return x

    @classmethod
    def dump_state_dict_tensors(cls, state_dict):
        dump = []

        for k, v in state_dict.items():
            if isinstance(v, torch.Tensor):
                v = v.tolist()

            dump.append((k, v))

        return dump

    @classmethod
    def load_state_dict_tensors(cls, dump):
        state_dict = OrderedDict()

        for k, v in dump:
            v = torch.tensor(v)

            state_dict[k] = v

        return state_dict

    def to_json(self, meta=None):
        construction_params = {
            'd_symbols': self.d_symbols,
            'd_model': self.d_model,
            'N': self.N,
            'heads': self.heads,
            'dropout': self.dropout,
            'lsr_epsilon': self.lsr_epsilon
        }

        state_dict = self.model.state_dict()

        model_dict = {
            'construction_params': construction_params,
            'state_dict': self.dump_state_dict_tensors(state_dict),
            'meta': meta
        }

        return json.dumps(model_dict)
    
    @classmethod
    def from_json(cls, s):
        model_dict = json.loads(s)
        construction_params = model_dict['construction_params']

        model = cls(**construction_params)
        model.model.load_state_dict(cls.load_state_dict_tensors(model_dict['state_dict']))

        model._meta = model_dict['meta']

        return model

    def save_file(self, filename, meta=None):
        with open(filename, 'w') as f:
            f.write(self.to_json(meta=meta))
    
    @classmethod
    def load_file(cls, filename):
        with open(filename, 'r') as f:
            j = f.read()
            return cls.from_json(j)
